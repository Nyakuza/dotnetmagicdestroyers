using Equipment;
namespace MagicDestroyers.Characters.Arcane
{
    public class Druid : Arcane
    {

        private readonly Armor DEFAULT_BODY_ARMOR = new LeatherVest();
        private readonly Staff DEFAULT_WEAPON = new Staff();
        
        public Druid()
            : this(Consts.Druid.NAME, 1)
        {
        }

        public Druid(string name, int level)
            : this(name, level, Consts.Druid.HEALTH_POINTS)
        {
        }

        public Druid(string name, int level, int healthPoints)
        {
            base.Name = name;
            base.Level = level;
            base.HealthPoints = healthPoints;
            base.ManaPoints = Consts.Druid.MANA_POINTS;
            base.Armor = DEFAULT_BODY_ARMOR;
            base.Weapon = DEFAULT_WEAPON;
            base.CharType = CharType.Arcane;
            base.IsAlive = true;
            base.Score = 0;

            counter++;
            CreationCofirmation();
        }


// abstract method implementations
        public override int Attack()
        {
            return this.Moonfire();
        }

        public override int SpecialAttack()
        {
            return this.Starburst();
        }

        public override int Defend()
        {
            return this.OneWithNature();
        }


        // class abilities
        public int Moonfire()
        {
            return base.Weapon.Damage + 10;
        }

        public int Starburst()
        {
            throw new NotImplementedException();
        }

        public int OneWithNature()
        {
            return base.Armor.ArmorPoints + 5;
        }
    }
}