namespace Equipment
{
    public abstract class Armor
    {
        int armorPoints;

        public int ArmorPoints {  
            get{ return armorPoints; }
            set{ armorPoints = value; }

        }
        
    }
}