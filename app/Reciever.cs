﻿using System;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;

namespace MagicDestroyers.QueueReciever
{
    public static class Reciever
    {
        // connection string to your Service Bus namespace
        static string connectionString = "Endpoint=sb://magicdestroyers.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=G9jOrWOyEmZ67oyg4mv3ytxbIyOzlUptal2LdlnCOo4=";

        // name of your Service Bus queue
        static string queueName = "twowaymessenger";


        // the client that owns the connection and can be used to create senders and receivers
        static ServiceBusClient client;

        // the processor that reads and processes messages from the queue
        static ServiceBusProcessor processor;

        public static String returnString = "";

        public static List<String> inputNames = new List<String>();

        public static int counter = 0;

        // handle received messages
        static async Task MessageHandler(ProcessMessageEventArgs args)
        {
            string body = args.Message.Body.ToString();
            returnString = body;
            Console.WriteLine($"Received: {body}");
            counter++;
            inputNames.Add(body);
            // complete the message. messages is deleted from the queue. 
            await args.CompleteMessageAsync(args.Message);

        }

        // handle any errors when receiving messages
        static Task ErrorHandler(ProcessErrorEventArgs args)
        {
            Console.WriteLine(args.Exception.ToString());
            return Task.CompletedTask;
        }

        public static async Task<List<string>> RecieverTask()
        {
            // The Service Bus client types are safe to cache and use as a singleton for the lifetime
            // of the application, which is best practice when messages are being published or read
            // regularly.
            //

            if (counter == 5) {
                return inputNames;
            }

            // Create the client object that will be used to create sender and receiver objects
            client = new ServiceBusClient(connectionString);

            // create a processor that we can use to process the messages
            processor = client.CreateProcessor(queueName, new ServiceBusProcessorOptions());

            try
            {
                // add handler to process messages
                processor.ProcessMessageAsync += MessageHandler;

                // add handler to process any errors
                processor.ProcessErrorAsync += ErrorHandler;

                // start processing 
                await processor.StartProcessingAsync();

                Console.WriteLine("Waiting for inputs");
                while (counter < 5) {
                    await Task.Delay(25);
                }

                // stop processing 
                Console.WriteLine("\nStopping the receiver...");
                await processor.StopProcessingAsync();
                Console.WriteLine("Stopped receiving messages");
            }
            finally
            {
                // Calling DisposeAsync on client types is required to ensure that network
                // resources and other unmanaged objects are properly cleaned up.
                await processor.DisposeAsync();
                await client.DisposeAsync();
                
            }
            return inputNames;
        }
    }
}